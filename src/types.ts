export interface Mission {
  agent: string
  country: string
  address: string
  date: string
}
