import { connectUrl } from './db'
import { MongoClient } from 'mongodb'

// Use connect method to connect to the Server
MongoClient.connect(connectUrl, async function(err, client) {
  const db = client.db('test')
  const data = require('./sample-data.json')
  await db.collection('missions').insertMany(data)
  client.close()
})
