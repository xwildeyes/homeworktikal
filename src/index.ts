import express from 'express'
import request from 'request'
import { getData, matrixApiKey, getMostIsolatedCountry } from './lib'
const app = express()

app.use(express.json())

app.get('/api/data', async (req, res) =>
  res.send(JSON.stringify(await getData()))
)

app.get('/api/countries-by-isolation', async (req, res) =>
  res.send(await getMostIsolatedCountry())
)
app.post('/api/find-closest', async (req, res) => {
  const data = await getData()
  const targetLocation = req.body['target-location']
  if (!targetLocation) return res.send({ error: 'no [target-location]' })
  const origins = data.map(d => d.address.replace(' ', '+'))
  const dest = targetLocation.replace(' ')
  const url = `https://maps.googleapis.com/maps/api/distancematrix/json?origins=${origins.join(
    '|'
  )}&destinations=${dest}&key=${matrixApiKey}`
  request(url, (err, response, body) => {
    const distanceData = JSON.parse(body)
    const distances = distanceData.rows.map(d => {
      if (d.elements[0].status === 'ZERO_RESULTS')
        return Number.MAX_SAFE_INTEGER
      return d.elements[0].distance.value
    }) // google returns results in the same order we sent them, we can use that to do the matching
    const places = data.map(({ address }, index) => ({
      address,
      distance: distances[index],
    }))

    places.sort((a, b) => (a.distance > b.distance ? 1 : -1))

    res.send(places)
  })
})

app.listen(3000, () => console.log('Listening on 3000...'))
