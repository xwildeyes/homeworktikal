import { readFileSync } from 'fs'
import { resolve } from 'path'
import { Mission } from './types'
import mongoose from 'mongoose'
import { getData } from './db'

export async function getMostIsolatedCountry() {
  const data = await getData()

  const numberOfMissionsByAgent = {}
  for (let index = 0; index < data.length; index++) {
    const { agent, country } = data[index]
    numberOfMissionsByAgent[agent] =
      typeof numberOfMissionsByAgent[agent] === 'number'
        ? numberOfMissionsByAgent[agent] + 1
        : 1
  }

  const countriesByIsolation = {}
  for (let index = 0; index < data.length; index++) {
    const { country, agent } = data[index]
    if (numberOfMissionsByAgent[agent] === 1) {
      countriesByIsolation[country] =
        typeof countriesByIsolation[country] === 'number'
          ? countriesByIsolation[country] + 1
          : 1
    }
  }
  let country
  let counter = 0
  for (const _country in countriesByIsolation) {
    if (countriesByIsolation.hasOwnProperty(_country)) {
      const count = countriesByIsolation[_country]
      if (count > counter) {
        counter = count
        country = _country
      }
    }
  }
  return country
}

export const matrixApiKey = 'AIzaSyBRQj20z0hG55962X-AYo8PLjzoQtlbtZQ'

export { getData }
