import mongoose, { Mongoose } from 'mongoose'
import { Mission } from './types'

export const connectUrl =
  'mongodb+srv://user:user@cluster0-b29qc.mongodb.net/test?retryWrites=true&w=majority'

const missionSchema = new mongoose.Schema({
  agent: String,
  country: String,
  address: String,
  date: String,
})

const Mission = mongoose.model('missions', missionSchema)

let db: Mongoose

export async function connect() {
  try {
    db = await mongoose.connect(connectUrl, {
      useNewUrlParser: true,
    })
  } catch (error) {
    console.error(error)
  }
}

export async function getData() {
  if (!db) await connect()

  return (await Mission.find().lean()) as Mission[]
}

if (require.main === module) {
  getData().then(console.log)
}
